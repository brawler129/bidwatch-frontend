import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms'
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { AddResourceComponent } from './add-resource/add-resource.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HttpClientModule } from '@angular/common/http';
import { ViewResourceDataComponent } from './view-resource-data/view-resource-data.component';
import { ViewDailyUpdatesComponent } from './view-daily-updates/view-daily-updates.component';
import { ViewResourceItemsComponent } from './view-resource-items/view-resource-items.component';
import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    AddResourceComponent,
    ViewResourceDataComponent,
    ViewDailyUpdatesComponent,
    ViewResourceItemsComponent,
    LoginComponent
  ],  
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
