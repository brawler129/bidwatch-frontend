import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddResourceComponent } from './add-resource/add-resource.component';
import { LoginComponent } from './login/login.component';
import { ViewDailyUpdatesComponent } from './view-daily-updates/view-daily-updates.component';
import { ViewResourceItemsComponent } from './view-resource-items/view-resource-items.component'
const routes: Routes = [
    { path: 'login', component: LoginComponent},
    { path: 'add-resource', component: AddResourceComponent },
    { path: 'daily-updates', component: ViewDailyUpdatesComponent},
    { path: 'resource-data/:pk/:set', component: ViewResourceItemsComponent},
    { path: '', redirectTo: 'daily-updates', pathMatch: 'full' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule{

}