import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment'

@Injectable({
    providedIn: 'root'
})
export class AuthService{
    accessTokenKey = 'bidwatch-access'
    refereshTokenKey = 'bidwatch-refresh'
    constructor(private http:HttpClient, private router:Router){

    }
    // Timeout
    private tokenRefreshTimeout

    // Access token
    accessToken:string;

    // Referesh token
    refreshToken:string;

    // Logged in indicator
    isLoggedIn:boolean;

    // Login subscription
    loginSubscription = new Subject<boolean>();

    refreshAccessToken(){
        // Refresh access token using refresh token
        console.log("refereshing");
        const url = environment.serverIP + 'bid-auth/refresh-token/';
        const formData: FormData = new FormData()
        formData.append('refresh', this.refreshToken)
        this.http.post(url, formData).subscribe(
            response =>{
                this.storeAccessTokens(response['access']);
                this.isLoggedIn = true;
                this.loginSubscription.next(true);
                this.startTokenRefreshTimer(this.getTokenExpDate(this.accessToken));
            },
            errorResp =>{
                this.isLoggedIn = false;
            }
        )
    }

    startTokenRefreshTimer(tokenExpDate:Date){
        // Start timer till 1 minute before expiry of access token
        const timeout = tokenExpDate.getTime() - Date.now() - (60 * 1000);
        this.tokenRefreshTimeout = setTimeout(() =>{
            this.refreshAccessToken();
        }, timeout)
    }

    autoLogin(){
        // Automatically login on starting app if possible otherwise redirect to login page.
        const refreshToken = localStorage.getItem(this.refereshTokenKey);
        const token = localStorage.getItem(this.accessTokenKey);
        if (!refreshToken){
            this.isLoggedIn = false;
            this.loginSubscription.next(false);
            this.router.navigate(['/login'])
            return;
        }

        if ( new Date > this.getTokenExpDate(refreshToken) ){
            // Refresh Token has expired
            this.isLoggedIn = false;
            this.loginSubscription.next(false);
            this.router.navigate(['/login']);
            return;
        }
        this.refreshToken = refreshToken;
        if ( new Date > this.getTokenExpDate(token)){
            this.refreshAccessToken();
        }
        else{
            this.accessToken = token;
            this.startTokenRefreshTimer(this.getTokenExpDate(this.accessToken));
            this.loginSubscription.next(true);
            this.isLoggedIn = true;
        }
        
        
    }

    sendLoginRequest(username:string, password:string){
        const url = environment.serverIP + 'bid-auth/obtain-tokens/';
        const formData:FormData = new FormData();
        formData.append('username', username);
        formData.append('password', password);
        return this.http.post(url, formData).pipe(map(
            response =>{
                this.storeAccessTokens(response['access']);
                this.storeRefreshTokens(response['refresh']);
                this.startTokenRefreshTimer(this.getTokenExpDate(this.accessToken));
                this.isLoggedIn = true;
                this.loginSubscription.next(true);
                return response;
            },
            (errorResp:HttpErrorResponse) =>{   
                return errorResp;
            }
        ))
    }

    storeAccessTokens(accessToken:string){
        // Store the tokens obtained from response
        this.accessToken = accessToken;
        localStorage.setItem(this.accessTokenKey, accessToken);
    }

    storeRefreshTokens(refreshToken:string){
        this.refreshToken = refreshToken;
        localStorage.setItem(this.refereshTokenKey, refreshToken);

    }


    getTokenExpDate(token:string){
        // Get expiry date of JWT token
        const tokenParts = token.split('.');
        const decodedPayload = JSON.parse(window.atob(tokenParts[1]))
        const expDate = new Date(decodedPayload.exp * 1000)
        return expDate
    }

    logout(){
        // Logout. Delete tokens from localstorage.
        this.accessToken = null;
        this.refreshToken = null;
        localStorage.removeItem(this.accessTokenKey);
        localStorage.removeItem(this.refereshTokenKey);
        this.isLoggedIn = false;
        this.loginSubscription.next(false);
    }
}