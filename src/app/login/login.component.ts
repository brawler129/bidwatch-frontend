import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  // Login form group
  loginForm:FormGroup;

  // Submitted checker
  submitted:Boolean;

  // Non-field error checker
  hasError:Boolean;

  // Non-field error message
  errorMsg:String;

  constructor(private formbuilder:FormBuilder, private authService:AuthService, private router:Router) { }

  ngOnInit(): void {
    this.hasError = false;
    this.submitted = false;
    this.loginForm = this.formbuilder.group({
      'username': ['', Validators.required],
      'password': ['', Validators.required]
    })

  }

  onLoginClick(){
    this.submitted = true;
    if (this.loginForm.invalid){
      return;
    }
    this.authService.sendLoginRequest(this.loginForm.controls['username'].value, this.loginForm.controls['password'].value).subscribe(
      response =>{
        this.router.navigate(['/daily-updates']);
      },
      (errorResp:HttpErrorResponse) =>{
        console.log(errorResp);
        this.hasError = true;
        this.errorMsg = errorResp.error.detail;
      }
    )
  }

}
