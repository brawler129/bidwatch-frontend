import { Component, Input, OnInit } from '@angular/core';
import { ResourceData } from '../models/resource-data.model';

@Component({
  selector: 'app-view-resource-data',
  templateUrl: './view-resource-data.component.html',
  styleUrls: ['./view-resource-data.component.scss']
})
export class ViewResourceDataComponent implements OnInit {
  @Input('resource-data')resourceData:ResourceData;
  // Indicate whether preview data is being loaded or not
  @Input('isPreview')isPreview:Boolean;
  // Only for complete data view not for preview.
  @Input('dateArray')dateArray:String[];

  constructor() { }

  ngOnInit(): void {
  }

}
