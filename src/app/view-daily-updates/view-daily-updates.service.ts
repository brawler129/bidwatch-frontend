import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment'
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AuthService } from '../login/auth.service';

export interface ResourceSummary{
    url: String;
    name: String;
    values_count:number;
    pk:number;
}

@Injectable({
    providedIn: 'root'
})
export class DailyUpdatesService{

    private resourceSummaries:ResourceSummary[];

    constructor(private http:HttpClient, private authService:AuthService){
    }

    getResourceDailyUpdates():Observable<any>{
        // Fetch daily update summaries for all resource
        this.resourceSummaries = [];
        const url = environment.serverIP + 'view/all_latest_data_count/';
        const httpOptions = {
            headers: new HttpHeaders({
                'Authorization': environment.tokenPrefix + this.authService.accessToken
            })
        }
        return this.http.get(url, httpOptions).pipe(map(
            (successResponse:any[])=>{
                for (let item of successResponse){
                    let resourceSummary:ResourceSummary = {
                        name: item.name,
                        url: item.url,
                        values_count: item.values_count,
                        pk: item.pk
                    };
                    this.resourceSummaries.push(resourceSummary);
                }
                return this.resourceSummaries;
            },
            errorResp =>{
                return errorResp;
            }
        ))
    }

}