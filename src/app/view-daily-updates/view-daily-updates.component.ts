import { Component, OnInit } from '@angular/core';
import { DailyUpdatesService, ResourceSummary } from './view-daily-updates.service';
import { faTrashAlt, faPen, faEye} from '@fortawesome/free-solid-svg-icons'
import { Router } from '@angular/router';
import { ResourceService } from '../services/resource.service';
import { AuthService } from '../login/auth.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-view-daily-updates',
  templateUrl: './view-daily-updates.component.html',
  styleUrls: ['./view-daily-updates.component.scss']
})
export class ViewDailyUpdatesComponent implements OnInit {

  // Boolean to indicate that data is loading
  public isLoading:Boolean;
  // Boolean to indicate there is an error
  public hasError:Boolean;
  // Error message to display
  public errorMsg:String;

  // Item selected for deletion (pk)
  public deletionContender:ResourceSummary;

  // Toggle deletion confirmation
  public confirmDelete:Boolean

  // Icons
  deleteIcon = faTrashAlt;
  editIcon = faPen;
  viewIcon = faEye;
  public resourceSummaries:ResourceSummary[];
  constructor(private dailyUpdatesService:DailyUpdatesService, private resourceService:ResourceService, private router:Router, private authService:AuthService) { }

  // Error Trigger
  ngOnInit(): void {
    this.isLoading = true;
    this.hasError = false;
    this.deletionContender = null;
    this.confirmDelete = false;
    this.dailyUpdatesService.getResourceDailyUpdates().subscribe(
      resourceSummaries=>{
        this.resourceSummaries = resourceSummaries;
        this.isLoading = false;
      },
      (error:HttpErrorResponse) =>{
        console.log(error)
        this.hasError = true;
        this.errorMsg = error.error;
      }
    )
  }

  onViewAllItemsClick(resourcePk:number){
    // View all data items of a resource
    this.router.navigate(['/resource-data', resourcePk.toString(), 'all'])
  }

  onDeleteResourceClick(resource:ResourceSummary){
    // Delete resource with resourcePk
    this.deletionContender = resource;
    this.confirmDelete = true;
  } 

  onViewNewItemsClick(resourcePk:number){
    this.router.navigate(['/resource-data', resourcePk.toString(), 'latest'])
  }

  onDeletionNoClick(){
    this.deletionContender = null;
    this.confirmDelete = false;
  }

  onDeletionYesClick(){
    this.resourceService.deleteResource(this.deletionContender.pk).subscribe(
      response =>{
        this.ngOnInit();
      },
      (errorResp: HttpErrorResponse) =>{
        this.hasError = true;
        this.errorMsg = errorResp.error;
        ;
      }
    )
  }
}
