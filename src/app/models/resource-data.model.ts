
export class ResourceData{
    public attributes: Attribute[];
    public dataset: DataSet;

    getAttributes(attributes_array:any[]): Attribute[]{
        // Deserialize attributes_array.
        let attributes:Attribute[] = [];
        for (let attribute of attributes_array){
            attributes.push(new Attribute(attribute.name, attribute.is_link));
        }
        return attributes;
    }

    getDataSet(data_array:any[]): DataSet{
        // Deserialize the data.
        let dataset:DataSet = {
            pages: []
        };
        for (const page_array of data_array){
            let page:Page = { rows : [] }
            for (const row_array of page_array){
                let row:Row = { data : [] }
                for (const index in row_array){
                    if (this.attributes[index].is_link){
                        let link_raw_value = row_array[index];
                        const link_array = link_raw_value.split('&LINK_TEXT=')
                        row.data.push(new Data(link_array[1], link_array[0]))
                    }
                    else{
                        row.data.push(new Data(row_array[index]))
                    }
                }
                page.rows.push(row)
            }
            dataset.pages.push(page)
        }
        return dataset
    }

    constructor(attributes:any[], data:any[]){
        this.attributes = this.getAttributes(attributes);
        this.dataset = this.getDataSet(data)
    }

}

interface DataSet{
    pages: Page[]
}

interface Page{
    rows: Row[]
}

interface Row{
    data: Data[]
}

class Data{
    constructor(public value:String, public link?:String){

    }

    is_link(){
        if (this.link == null){
            return false;
        }
        else{
            return true;
        }
    }
}

class Attribute{
    constructor(public name:String, public is_link:Boolean){

    }

}
