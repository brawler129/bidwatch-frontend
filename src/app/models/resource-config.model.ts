export class ResourceConfig{
    private attributes:Attribute[]
    private events:any[]

    getAttributes(attributes_array:any[]):Attribute[]{
        let attributes:Attribute[] = []
        for (let attribute of attributes_array){
            attributes.push(new Attribute(attribute['name'], attribute['path'], attribute['is_link']))
        }
        return attributes
    }

    getEvents(eventsArray:any[]):any[]{
        let events:any[] = []
        for (let event of eventsArray){
            if (event.type === 'form'){
                events.push(new FormEvent(event.type, event.fields, event.submit_button_path))
            }
            else if (event.type === 'click'){
                events.push(new ClickEvent(event.type, event.button_path))
            }
        }

        return events

    }

    constructor(private name:String, private url:String, attributes_array:any[], private next_page_path:String, private container_path:String, events_array:any[], private run_on:String){
        this.attributes = this.getAttributes(attributes_array)
        this.events = this.getEvents(events_array)
    }

    serialize(){
        let attributes:any[] = [];
        let events:any[] = [];
        for (let attribute of this.attributes){
            attributes.push(attribute.serialize());
        }
        for (let event of this.events){
            events.push(event.serialize())
        }
        let data = {
            "url": this.url,
            "name": this.name,
            "run_on": this.run_on,
            "attributes": attributes
        }
        if (this.next_page_path.length > 0){
            data['next_page_path'] = this.next_page_path;
        }
        if (this.container_path.length > 0){
            data['container_path'] = this.container_path;
        }

        if (this.events.length > 0){
            data['events'] = events
        }

        return data
    }
}

class ClickEvent{

    constructor(private type:String, private button_path:String){

    }

    serialize(){
        let data = {
            "type": this.type,
            "click_action": {
                "button_path": this.button_path
            }
        }
        return data
    }

}

class FormEvent{
    private fields:any[] = []

    getFormFields(fields_array):any[]{
        let fields:any[] = [];
        for (let field of fields_array){
            if (field.type === 'text'){
                fields.push(new TextField(field.type, field.path, field.value))
            } 
            else if (field.type === 'multitext'){
                fields.push(new MultiTextField(field.type, field.path, field.value))
            }
            else if (field.type === 'select'){
                fields.push(new SelectField(field.type, field.path, field.value))
            }
        }
        return fields;
    }

    constructor(private type:String, fields:any[], private submit_button_path:String){
        this.fields = this.getFormFields(fields)
    }

    serialize(){
        let fields:any[] = []
        for (let field of this.fields){
            fields.push(field.serialize());
        }
        let data = {
            "type": this.type,
            "form_action": {
                "fields": fields
            }
        };
        if (this.submit_button_path.length > 0){
            data.form_action["submit_button_path"] = this.submit_button_path;
        };
        return data;
    }
}

class MultiTextField{
    constructor(private type:String, private path:String, private value:String){
    }

    serialize(){
        let values:string[] = [];
        values = this.value.split(',');
        for (let i in values){
            // remove ' ' from first character if present
            if (values[i].charAt(0) === ' '){
                values[i] = values[i].substring(1)
            }
        };
        let data = {
            "type": this.type,
            "path": this.path,
            "value": {
                "value": values
            }
        };
        return data;
    }
}

class TextField{
    constructor(private type:String, private path:String, private value:String){

    }

    serialize(){
        let data = {
            "type": this.type,
            "path": this.path,
            "value": {
                "value": this.value
            }
        };
        return data;
    }
}

class SelectField{
    constructor(private type:String, private path:String, private value:String){

    }

    serialize(){
        let data = {
            "type": this.type,
            "path": this.path,
            "value": {
                "value": this.value
            }
        };
        return data;
    }
}


class Attribute{

    constructor(private name:String, private path:String, private is_link:Boolean){

    }

    serialize(){
        let data = {
            "name": this.name,
            "path": this.path,
            "is_link": this.is_link
        }

        return data
    }
}