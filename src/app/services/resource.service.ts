import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { AuthService } from '../login/auth.service';
import { ResourceData } from '../models/resource-data.model';


export interface ResourceDetails{
    attributes: any[];
    url:String;
    name:String;
    pk:number;
}

export interface ResourcePageData{
    page:number;
    totalCount:number;
    values: any[];
    dates: String[];
}

interface DataResult{
    values:any[],
    dates: String[]
}

@Injectable({
    providedIn: 'root'
})
export class ResourceService{
    private resourceDetails:ResourceDetails;
    constructor(private http:HttpClient, private authService:AuthService){
    }

    getResourceDetails(id:number){
        // Get Resource details without event
        const url = environment.serverIP + 'resource/' + id.toString();
        const httpOptions = {
            headers: new HttpHeaders({
                'Authorization': environment.tokenPrefix + this.authService.accessToken
            })
        }
        return this.http.get(url, httpOptions).pipe(map(
            resourceDetails =>{
                let resource:ResourceDetails = {
                    name: resourceDetails['name'],
                    url: resourceDetails['url'],
                    pk: resourceDetails['pk'],
                    attributes: resourceDetails['attributes']
                }
                this.resourceDetails = resource;
                return this.resourceDetails;
            },
            errorResp =>{
                return errorResp;

            }
        ))
    }

    getResourceItems(pageNo:number, pk:number, isLatest:Boolean){
        // Get all resource data. Split by pages. Get data for pageNo
        let url = environment.serverIP + 'view/all_data/' + pk.toString() + '?page=' + pageNo.toString();
        if (isLatest){
            url += '&latest=1'
        }
        const httpOptions = {
            headers: new HttpHeaders({
                'Authorization': environment.tokenPrefix + this.authService.accessToken
            })
        }
        return this.http.get(url, httpOptions).pipe(map(
            resourceItems =>{
                let dataResult:DataResult = this.getDataResult(resourceItems['results'])
                let resourceData:ResourcePageData = {
                    page: pageNo,
                    totalCount: resourceItems['count'],
                    values: [dataResult.values],
                    dates: dataResult.dates
                }
                return resourceData;
            },
            errorResp =>{
                return errorResp;
            }
        ))
    }

    getDataResult(results:any[]):DataResult{
        // Modify the raw results to suit front end needs.
        let dataResult:DataResult = {
            values: [],
            dates: []
        }
        for (let item of results){
            dataResult.values.push(item['value']);
            dataResult.dates.push(item['added_on']);
        }
        return dataResult;
    }

    deleteResource(resourcePk:number){
        // Delete Resource
        const url = environment.serverIP + 'resource/' + resourcePk.toString();
        const httpOptions = {
            headers: new HttpHeaders({
                'Authorization': environment.tokenPrefix + this.authService.accessToken
            })
        }
        return this.http.delete(url, httpOptions);
    }

}