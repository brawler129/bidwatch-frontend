import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray, Form } from '@angular/forms'
import { Router } from '@angular/router';
import { faTrashAlt, faMinus } from '@fortawesome/free-solid-svg-icons'
import { stringify } from 'querystring';
import { ResourceData } from '../models/resource-data.model';
import { AddResourceService } from './add-resource.service';


@Component({
  selector: 'app-add-resource',
  templateUrl: './add-resource.component.html',
  styleUrls: ['./add-resource.component.scss']
})
export class AddResourceComponent implements OnInit {
  //FONTAWESOME
  deleteIcon = faTrashAlt;
  minusIcon = faMinus;

  // Event Templates
  @ViewChild('clickEvent') private clickEvent: TemplateRef<any>;
  @ViewChild('formEvent') private formEvent: TemplateRef<any>;

  // Form Field Templates
  @ViewChild('textField') private textField: TemplateRef<any>;
  @ViewChild('multiTextField') private multiTextField: TemplateRef<any>;
  @ViewChild('selectField') private selectField: TemplateRef<any>;

  // Attribute Template
  @ViewChild('attributeRef') public attributeRef: TemplateRef<any>;

  // Boolean to indicate that form is submitted
  public submitted:Boolean;

  // Boolean to indicate that there is not even a single attribute. A non-field error indicator
  public attributeMissingError:Boolean;

  // Boolean to indicate that there is not even a single form field in at least one form event. A non-field error indicator
  public formFieldMissingError:Boolean;

  // Boolean to indicat that there is an error in the server caused by the config data.
  public serverError:Boolean;

  // Boolean to indicate that the data has been received
  public isDataReceived:Boolean;

  // Boolean to indicate that request is getting processed
  public isLoading:Boolean;

  // Boolean to indicate that resource is being saved
  public isSaving:Boolean;

  // Server Error message
  public serverErrorMsg:String;

  // ResourceDataPreview
  public resourceData:ResourceData;

  public resourceForm:FormGroup
  public eventTypes:String[] // Array containing types of event as key string.
  public formFieldTypes:String[] // Array containing types of form fields as key strings


  constructor(private formbuilder:FormBuilder, private router:Router, private addResourceService:AddResourceService) { }

  ngOnInit(): void {
    this.eventTypes = ['click', 'form']
    this.formFieldTypes = ['text', 'select', 'multitext']
    this.submitted = false;
    this.attributeMissingError = false;
    this.serverError = false;
    this.isDataReceived = false;
    this.isLoading = false;
    this.isSaving = false;
    this.resourceForm = this.formbuilder.group({
      'name':['', Validators.required],
      'url': ['', Validators.required],
      'container_path':[''],
      'next_page_path':[''],
      'run_on': ['', Validators.required],
      'attributes': new FormArray([]),
      'events': new FormArray([])
    })
  }


  get events(){
    // Shortcut to return events
    return this.resourceForm.controls['events'] as FormArray;
  }

  get attributes(){
    // Shortcut to return attributes.
    return this.resourceForm.controls['attributes'] as FormArray;
  }


  getEvent(index:number){
    // Get event at index
    return this.events.get(index.toString()) as FormArray;
  }

  getFormFields(index:number){
    // Get Form Fields for Form event at index.
    return this.getEvent(index).controls['fields'] as FormArray;
  }


  setEventTemplate(event){
    // Return tag of event template to load depending of the type of event
    if (event.controls['type'].value === 'click'){
      return this.clickEvent;
    }
    else{
      return this.formEvent;
    }
  }

  setFormFieldTemplate(formField){
    // Return tag of form field to load depending on the type of form field.
    const fieldType = formField.controls['type'].value; 
    if (fieldType === 'text'){
      return this.textField;
    }
    else if (fieldType === 'multitext'){
      return this.multiTextField;
    }
    else if (fieldType === 'select'){
      return this.selectField;
    }
  }

  addEvent(){
    // Add event to resource config.
    const event_type = (document.getElementById('eventSelector') as HTMLInputElement).value;
    if(event_type === 'click'){
      this.events.push(this.getClickActionForm());
    }
    else if(event_type === 'form'){
      this.events.push(this.getFormActionForm());
    }
    else{
      return
    }
    return
  }

  deleteEvent(index:number){
    // Delete event from event list
    this.events.removeAt(index);
  }

  addFormField(index:number){
    // Add form field to Form event at index.
    const formFieldType = (document.getElementById('formFieldSelector' + index) as HTMLInputElement).value;
    if (formFieldType === 'text'){
      this.getFormFields(index).push(this.getTextFormFieldForm());
    }
    else if (formFieldType === 'multitext'){
      this.getFormFields(index).push(this.getMultiTextFormFieldForm());
    }
    else if (formFieldType === 'select'){
      this.getFormFields(index).push(this.getSelectFormFieldForm());
    }
    else{
      return;
    }
    return;
  }

  deleteFormField(field_index:number, event_index:number){
    // Delete form field with index as event_index, for event at event_index.
    let fields = this.getFormFields(event_index);
    fields.removeAt(field_index);
  }

  addAttribute(){
    // Add attribute form to attributes list
    this.attributes.push(this.getAttributeForm());
  }

  deleteAttribute(index:number){
    // Delete attribute at index.
    this.attributes.removeAt(index);
  }

  //---------------------------------------------------------------------------//

  // ATTRIBUTE FORM
  getAttributeForm():FormGroup{
    // Get form group for an attribute. 
    return this.formbuilder.group({
      'name': ['', Validators.required],
      'path': ['', Validators.required],
      'is_link': [false, Validators.required]
    });
  }

  //---------------------------------------------------------------------------//

  // EVENTS FORM
  getClickActionForm():FormGroup{
    // Get form group for click action.
    return this.formbuilder.group({
      'type': ['click', Validators.required],
      'button_path': ['', Validators.required]
    })
  }

  getFormActionForm():FormGroup{
    // Get form group for form action.
    return this.formbuilder.group({
      'type': ['form', Validators.required],
      'submit_button_path': [''],
      'fields': new FormArray([])
    })
  }

  //-----------------------------------------------------------------------------//
  // FORM FIELDS FORM
  getTextFormFieldForm():FormGroup{
    // Get form group for text field.
    return this.formbuilder.group({
      'type': ['text', Validators.required],
      'path': ['', Validators.required],
      'value': ['', Validators.required]
    })
  }

  getSelectFormFieldForm():FormGroup{
    // Get form group for select field.
    return this.formbuilder.group({
      'type': ['select', Validators.required],
      'path': ['', Validators.required],
      'value': ['', Validators.required]
    })
  }

  getMultiTextFormFieldForm():FormGroup{
    // Get form group for multi text field.
    return this.formbuilder.group({
      'type': ['multitext', Validators.required],
      'path': ['', Validators.required],
      'value': ['', Validators.required]
    })
  }


  //-----------------------------------------------------------------------//

  isAttributesMissing():Boolean{
    // Whether there is at least one attribute in the form. At least one attribute is necessary
    if (this.attributes.length < 1){
      return true;
    }
    return false;
  }

  isFormFieldMissing():Boolean{
    // Check if there is a form event with no form fields. At least one form field per form event is necessary
    let events = this.resourceForm.value.events
    for (let event of events){
      if (event.type === 'form'){
        if (event.fields.length < 1){
          return true;
        }
      }
    }
    return false;

  }

  onSubmitButtonClick(){
    // After preview click. Check for field and non-field errors.
    this.submitted = true;
    this.isDataReceived = false;
    if (this.resourceForm.invalid){
      return;
    }

    if (this.isFormFieldMissing()){
      this.formFieldMissingError = true;
      return;
    }
    this.formFieldMissingError = false;

    if (this.isAttributesMissing()){
      this.attributeMissingError = true;
      return;
    }
    this.attributeMissingError = false;


    this.isLoading = true;
    this.resourceForm.disable();
    this.addResourceService.previewResourceData(this.resourceForm.value).subscribe(
      successResp =>{
        this.resourceForm.enable();
        this.serverError = false;
        this.resourceData = new ResourceData(successResp.attributes, successResp.data);
        this.isLoading = false;
        this.isDataReceived = true;
      },
      errorResp =>{
        this.resourceForm.enable();
        this.serverError = true;
        this.isLoading = false;
        this.serverErrorMsg = this.getErrorMessage(errorResp);
      }

    )
  }


  onSaveResourceClick(){
    // Send request to save the resource.
    this.isSaving = true;
    this.resourceForm.disable();
    this.addResourceService.saveResourceData().subscribe(
      successResp =>{
        this.router.navigate(['daily-updates'])
      },
      errorResp =>{
        this.isSaving = false;
        this.resourceForm.enable();
        this.serverError = true;
        this.serverErrorMsg = this.getErrorMessage(errorResp);
      }
    )
  }

  getErrorMessage(errorResponseObject:HttpErrorResponse){
    return errorResponseObject.error;
  }
}
