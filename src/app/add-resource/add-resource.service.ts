import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from '../../environments/environment'
import { ResourceConfig } from '../models/resource-config.model'
import { Observable } from 'rxjs';
import { AuthService } from '../login/auth.service';

@Injectable({
    providedIn: 'root'
})
export class AddResourceService{

    constructor(private http:HttpClient, private authService:AuthService){

    }

    private serialized_data
    getResourcePostData(formData){
        let resource_config = new ResourceConfig(formData.name,
            formData.url,
            formData.attributes, 
            formData.next_page_path, 
            formData.container_path, 
            formData.events,
            formData.run_on
        );
        this.serialized_data = resource_config.serialize();
    }

    previewResourceData(formData):Observable<any>{
        // POST Resource config data and get Resource data preview.
        const previewURL = environment.serverIP + 'resource/preview';
        this.getResourcePostData(formData);
        const httpOptions = {
            headers: new HttpHeaders({
                'Authorization': environment.tokenPrefix + this.authService.accessToken
            })
        }
        return this.http.post(previewURL, this.serialized_data, httpOptions);
    }

    saveResourceData():Observable<any>{
        // POST Resouce config data and save it in the server and return complete data.
        const saveURL = environment.serverIP + 'resource/save';
        const httpOptions = {
            headers: new HttpHeaders({
                'Authorization': environment.tokenPrefix + this.authService.accessToken
            })
        }
        console.log(this.serialized_data)
        return this.http.post(saveURL, this.serialized_data, httpOptions);
    }

}