import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ResourceData } from '../models/resource-data.model';
import { ResourceDetails, ResourcePageData, ResourceService } from '../services/resource.service'
@Component({
  selector: 'app-view-resource-items',
  templateUrl: './view-resource-items.component.html',
  styleUrls: ['./view-resource-items.component.scss']
})
export class ViewResourceItemsComponent implements OnInit {
  // Primary key of the resource.
  pk:number;
  // Indicates whether page is data is loading
  isLoading:Boolean;
  // Indicates pageno entered is out of limit
  pageOutOfLimitError:Boolean;
  // Resource data for current page
  resourceData:ResourceData;
  // Attributes of the resource
  attributes: any[];

  resourceDetails:ResourceDetails;
  pageSize:number = 30;
  // Total pages of data
  totalPages:number;
  // Total data items count
  totalItemCount:number;
  // Current Page
  currentPage:number;
  // Added on Dates Array
  datesArray: String[];

  // Value of page no input
  pageNoInputValue:HTMLInputElement = document.getElementById('pageSelector') as HTMLInputElement;

  // Whether to display all or latest only
  isLatest:Boolean;

  constructor(private resourceService:ResourceService, private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.pageOutOfLimitError = false;
    this.route.paramMap.subscribe(params =>{
      this.pk = parseInt(params.get('pk'))
      const set = params.get('set')
      this.isLatest = set === 'latest'? true : false;
      this.resourceService.getResourceDetails(this.pk).subscribe(
        (resourceDetails:ResourceDetails) =>{
          this.resourceDetails = resourceDetails;
          // Load First page
          this.resourceService.getResourceItems(1, this.resourceDetails.pk, this.isLatest).subscribe(
            (resourcePageData:ResourcePageData)=>{
              this.currentPage = resourcePageData.page;
              this.totalItemCount = resourcePageData.totalCount;
              this.totalPages = this.getTotalPages();
              this.datesArray = resourcePageData.dates;
              this.resourceData = new ResourceData(this.resourceDetails.attributes, resourcePageData.values);
              this.isLoading = false;
            }
          )
        } 
      )
    })
  }


  getTotalPages(){
    // Get total pages count based on number of data items and page size.
    return Math.ceil(this.totalItemCount / this.pageSize);
  }

  getPreviousPage(){
    // Get previous data page
    this.getDataPage(this.currentPage - 1)
  }

  getNextPage(){
    // Get next data page
    this.getDataPage(this.currentPage + 1)
  }

  goToSelectedPageClick(){
    // Get page entered or display error if over limit.
    const pageNo = parseInt((document.getElementById('pageSelector') as HTMLInputElement).value);
    if ((pageNo > this.totalPages) || (pageNo < 1)){
      this.pageOutOfLimitError = true;
    }
    else{
      this.getDataPage(pageNo);
    }
  }


  getDataPage(pageNo:number){
    // Get page with page no
    this.isLoading = true;
    this.resourceService.getResourceItems(pageNo, this.resourceDetails.pk, this.isLatest).subscribe(
      (resourcePageData:ResourcePageData)=>{
        this.datesArray = resourcePageData.dates;
        this.resourceData = new ResourceData(this.resourceDetails.attributes, resourcePageData.values);
        this.currentPage = resourcePageData.page;
        this.pageOutOfLimitError = false;
        this.isLoading = false;

      }
    )
  }

}
