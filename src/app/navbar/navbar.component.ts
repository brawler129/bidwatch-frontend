import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../login/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  isLoggedIn:boolean;
  constructor(private authService:AuthService, private router:Router) { }

  ngOnInit(): void {
    this.isLoggedIn = this.authService.isLoggedIn;
    this.authService.loginSubscription.subscribe(
      isLoggedIn =>{
        this.isLoggedIn = isLoggedIn;
      }
    )
  }

  onDailyUpdatesClick(){
    // Navigate to daily updates
    this.router.navigate(['/daily-updates'])
  }

  onAddResourceClick(){
    // Navigate to add resource
    this.router.navigate(['/add-resource'])
  }

  onLogoutClick(){
    // Logout user
    this.authService.logout();
    this.router.navigate(['/login']);
  }
}
